var fs   = require('fs'),
	path = require('path'),
	templatesDir = './js/templates/';

module.exports = function () {
	var files, results;

	files = fs.readdirSync(templatesDir);
	results = files.filter(function (el) {
		return el.indexOf('.html') > -1;
	}).map(function (el, index) {
		return 'exports["' + path.basename(el, '.html') + '"] = ' +
		'_.template(\'' + fs.readFileSync(templatesDir + el, 'utf8')
		.replace(/[\r\t\n]/g, "").replace(/'/g, '\'') + '\');';
	});

	results.unshift('var _ = require("underscore");');

	fs.writeFileSync(templatesDir + 'index.js', results.join('\n'));
};
