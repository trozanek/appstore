var fs = require('fs');

module.exports = function (source, target) {
	fs.writeFileSync(target, fs.readFileSync(source));
};
