var Backbone = require('backbone'),
	_        = require('underscore'),
	App      = require('../models/App.js'),
	Apps;

Apps = Backbone.Collection.extend({
	initialize: function () {
		var self = this;
		this.loaded = false;

		this.bind('add', function () {
			var model = this.models[this.models.length - 1];
			if (Store.settings.isGenentech && (model.get('SHOW_IN_STORE') === 'N')) {
				this.remove(model);
			}
			if (Store.settings.isRoche && (model.get('SHOW_IN_ROCHE') === 'N')) {
				this.remove(model);
			}
		});
	},
	comparator: function (app) {
		return app.get('APP_INDEX');
	},
	load: function (callback) {
		console.log("Loading applications data...");
		var self = this;
		this.trigger('beforeload');
		$.ajax({
			url: Store.settings.appListURL,
			dataType: 'json',
			cache: false,
			success: function (data) {
				console.log("Application data loaded");
				console.dir(data);
				//keep a private copy of the request - why? I'll leave it just in case.
				Store._apps = data.resultSet.results;
				self.add(Store._apps);

				self.loaded = true;
				if (callback) {
					callback();
				}
				self.trigger('afterload');

			},
			error: function (data) {
				console.log('ERROR IN AJAX :: APP_LIST');
				console.log(data);
			}
		});

	},
	model: App
});

module.exports = Apps;
