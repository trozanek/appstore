var _         = require('underscore'),
	Backbone  = require('backbone'),
	getCookie = require('../helper/get-cookie'),
	getFromCookie, getFromStorage;

getFromCookie = function () {
	var value = getCookie('installedApps');
	if (value !== null) {
		return JSON.parse(value);
	}
	return null;
};

getFromStorage = function () {
	if (!window.localStorage) {
		throw new Error("No local storage!");
	}
	var data = window.localStorage.getItem("installedApps");
	return data ? JSON.parse(data) : null;
};

module.exports = (function () {
	var AppsManager;
	return function () {
		if (!AppsManager) {
			AppsManager = _.extend({
				installedApps: {},
				loaded: false,
				loadAppsInfo: function (callback) {
					console.log("LOAD APS INFO STARTED");
					var self = this;
					this.trigger('beforeload');
					this._loadRemoteData(function (err) {
						self._loadLocalData();
						self.loaded = true;
						self.trigger('afterload');
						self.trigger('change');
						if (callback) {
							callback(err);
						}
					});
				},
				_loadRemoteData: function (callback) {
					var userid = Store.settings.userid, esbOptions, parseData,
						self = this;

					parseData = function (results) {
						var res = {};
						_.each(results, function (app) {
							if (app.CURRENT_APP_VERSION) {
								// if the model matches or the client is a web browser
								if ((app.MODEL.toLowerCase() === Store.settings.client) ||
									(Store.settings.client === 'web')) {
									res[app.APPLICATION_NAME] = {
										version: app.CURRENT_APP_VERSION,
										data: app,
										time: 0
									};
								}
							}
						});
						return res;
					};

					esbOptions = {
						cType: 'json',
						NSName: 'GneENT_Common.Services:genericSQLAdapter',
						apiKey: Store.settings.apiKey,
						//searchKey: 'IAPPDEVICESFORUSER:wisniewo',// + self.settings.userid,
						searchKey: 'IAPPDEVICESFORUSER:' + Store.settings.userid,
						searchConnectionFlag: 'true',
						srcPackageName: 'GneENT_Mobility'
					};
					$.ajax({
						url: Store.settings.ESBBase,
						data: esbOptions,
						dataType: 'jsonp',
						timeout: 5000,
						xhrFields: {
							withCredentials: true
						},
						success: function (data) {
							if (data.response && data.response.id === "error") {
								console.log("ESB ERROR");
								console.log(data.response.message);
								callback(new Error("ESB ERROR " + data.response.message));
								return;
							}
							console.log("ESB SUCCESS, parsing data");
							console.dir(data);
							self.installedApps = parseData(data.resultSet && data.resultSet.results ? data.resultSet.results : []);
							callback();
						},
						error: function (data) {
							console.log('ERROR IN AJAX :: ESB_CALL', data);
							callback(new Error("Error in AJAX ESB CALL"));
						}
					});
				},
				_loadLocalData: function () {
					var data, remoteData = this.installedApps;
					console.log("LOAD LOCAL DATA");

					data = window.localStorage ? getFromStorage() : getFromCookie();
					if (data) {
						_.each(_.keys(data), function (key) {
							if (remoteData[key]) {
								if (remoteData[key].time > data[key].time) {
									//local information is outdated;
									return;
								}
							}
							remoteData[key] = data[key];
						});
					}
				},
				_saveData: function () {
					var string = JSON.stringify(this.installedApps);
					if (window.localStorage) {
						window.localStorage.setItem("installedApps", string);
					} else {
						document.cookie = "installedApps=" + string;
					}
				},
				isInstalled: function (appName) {
					return _.indexOf(_.keys(this.installedApps), appName) > -1;
				},
				isUpToDate: function (appName, targetVersion) {
					return this.installedApps[appName] ?
						this.installedApps[appName].version >= targetVersion : false;
				},
				setInstalled: function (appName, version, suppress) {
					var self = this;
					if (_.isArray(appName)) {
						_.each(appName, function (app) {
							self.setInstalled(app.name, app.version, true);
						});
					} else {
						if (this.installedApps[appName]) {
							this.installedApps[appName].version = version;
							this.installedApps[appName].time = (new Date()).getTime();
						} else {
							this.installedApps[appName] = {
								version: version,
								time: (new Date()).getTime(),
								data: {}
							};
						}
					}

					this._saveData();
					if (!suppress) {
						this.trigger('change');
					}
				}
			}, Backbone.Events);
		}
		return AppsManager;
	};
}());
