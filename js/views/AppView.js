var _         = require('underscore'),
	BaseView  = require('./BaseView.js'),
	App       = require('../models/App.js'),
	Templates = require('../templates'),
	setBtnCls = require('../helper/set-button-class');

module.exports = BaseView.extend({
	model: App,
	tagName: 'li',
	template: Templates.AppView,
	initialize: function () {
		this.app = this.options.app;
		if (!this.app) {
			console.dir(this);
			throw new Error("No application model passed to AppView");
		}
		this.hidden = false;
	},
	render: function () {
		this.$el.html(this.template(this.app.toJSON()));
		this.$el.attr('id', this.app.get('APPLICATION_NAME'));
		setTimeout(_.bind(function () {
			this.button = this.$el.children('.install-button');
			if (this.appsManager.loaded) {
				this._setButtonText();
			} else {
				this.appsManager.on('change', _.bind(this._setButtonText, this));
			}
			this._bindListeners();
		}, this), 1);
		return this;
	},
	_bindListeners: function () {
		var self = this, install = _.bind(function () {
			this.appsManager.setInstalled(
				this.app.get('APPLICATION_NAME'),
				this.app.get('APPLICATION_VERSION')
			);
			window.location.href = self.app.get('APPLICATION_INSTALL_URL');
		}, this);

		this.button.on('click', function (e) {
			e.preventDefault();
			e.stopPropagation();
			if (Store.settings.isWeb) {
				Store.overlay.show(self.app, install);
			} else {
				install();
			}
			return false;
		});

		// if (Store.settings.isWeb) {
		this.$el.on('click', function (e) {
			e.preventDefault();
			Store.navigate("#apps/" + self.app.get('APPLICATION_NAME'));
		});
		// }
	},
	hide: function () {
		this.hidden = true;
		this.$el.hide();
	},
	show: function () {
		this.hidden = false;
		this.$el.show();
	},
	_setButtonText: function () {
		var appsManager = this.appsManager,
			appName = this.app.get('APPLICATION_NAME'),
			appVer = this.app.get('APPLICATION_VERSION');

		if (appsManager.isInstalled(appName)) {
			if (!appsManager.isUpToDate(appName, appVer)) {
				setBtnCls(this.button, 'update-button');
				this.button.text(this.app.get('UPDATE_TEXT'));
			} else {
				setBtnCls(this.button, 'reinstall-button');
				this.button.text(this.app.get('REINSTALL_TEXT'));
			}
		} else {
			setBtnCls(this.button, '');
			this.button.text(this.app.get('INSTALL_TEXT'));
		}
	}
});
