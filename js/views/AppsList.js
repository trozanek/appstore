var _              = require('underscore'),
	BaseView       = require('./BaseView.js'),
	AppView        = require('./AppView.js'),
	QuickSearch    = require('./QuickSearch'),
	Loader         = require('./Loader'),
	Templates      = require('../templates'),
	helper         = require('../helper'),
	bindSwipe      = require('../helper/bind-swipe'),
	hasTransform   = require('../helper/has-transform'),
	setMarker;

setMarker = function (number) {
	this.markers.removeClass('active');
	this.markers.eq(number).addClass('active');
};

module.exports = BaseView.extend({
	tagName: 'div',
	className: 'apps-showroom',
	events: {
		'click a.left-nav-button' : 'prevPage',
		'click a.right-nav-button' : 'nextPage'
	},
	template: Templates.AppsList,
	catOptionTmpl: _.template('<option value="<%=cat%>"><%=cat%></option>'),
	initialize: function () {
		this.page = 0;
		this.maxPage = 0;
		this.appsViews = [];
		this.quickSearch = new QuickSearch({
			appsList: this
		});
		this.categories = ['All'];
		this.category = 'All';
		this.search = '';
		if (Store.apps.loaded) {
			this._populate();
		} else {
			Store.apps.on('afterload', _.bind(this._populate, this));
		}

		Store.on('orientationchange', _.bind(this._adjustList, this));
	},
	_populate: function () {
		var length = Store.apps.length, half = Math.ceil(length / 2);
		//some fancy sorting to display apps in proper sequence
		if (!Store.settings.isIPhone) {
			Store.apps.each(_.bind(function (app, index) {
				var newInd, ind = index + 1, counter = Math.floor(index / 2);
				if (ind % 2 === 0) {
					newInd = counter + half;
				} else {
					newInd = counter;
				}
				this.appsViews[newInd] = new AppView({
					app: app
				});
			}, this));
		} else {
			Store.apps.each(_.bind(function (app, index) {
				this.appsViews.push(new AppView({
					app: app
				}));
			}, this));
		}
		_.each(this.appsViews, _.bind(function (appView) {
			var tag = appView.app.get('APP_TAGS');
			if (_.indexOf(this.categories, tag) === -1) {
				this.categories.push(tag);
			}
		}, this));
		this.categories.sort();
	},
	_applyFilters: function () {
		var filter = _.bind(function (appView) {
			return appView.app.filterByCategory(this.category) && appView.app.filterBySearch(this.search);
		}, this);
		_.each(this.appsViews, function (appView) {
			appView[filter(appView) ? 'show' : 'hide']();
		});
		this._adjustList();
	},
	_adjustList: function (dontReset) {
		if (Store.settings.isIPhone) {
			return;
		}
		var width, containerWidth = this.appsWrapper.width(),
			visibleCount = _.filter(this.appsViews, function (app) {
				return app.hidden !== true;
			}).length;
		if (visibleCount <= 3) {
			width = containerWidth;
		} else {
			width = Math.ceil(visibleCount / 2) * (containerWidth / 3);
		}
		this.appsContainer.css('width', width);

		this.maxPage = Math.ceil(visibleCount / 6) - 1;

		if (dontReset !== true) {
			this._reset();
			this._setMarkers();
		}
	},
	_reset: function () {
		this.page = 0;
		this._slideTo(this.page);
		if (this.maxPage <= 0) {
			this.rightNav.addClass('disabled');
		} else {
			this.rightNav.removeClass('disabled');
		}
		this.leftNav.addClass('disabled');
	},
	_setMarkers: function () {
		var markers = [];
		for (var i = 0; i <= this.maxPage; i++) {
			markers.push($('<a/>', {
				'class': i === 0 ? 'active' : ''
			}));
		}
		this.markersContainer.html('');
		this.markersContainer.append.apply(this.markersContainer, markers);
		this.markers = this.markersContainer.children('a');
	},
	filterCategory: function (category) {
		this.categoryFilter.children('span').text(category);
		this.category = category;
		this._applyFilters();
	},
	filterQuickFind: function (text) {
		this.search = text;
		this._applyFilters();
	},
	_renderApplications: function () {
		this.appsContainer.html('');
		this.appsContainer.append.apply(this.appsContainer,
			_.map(this.appsViews, function (appView) {
				return appView.render().$el;
			}));
		this._adjustList();
	},
	_renderCategories: function () {
		var tmpl = this.catOptionTmpl;
		this.categorySelect.append.apply(
			this.categorySelect,
			_.map(this.categories, function (cat) {
				return tmpl({
					cat: cat
				});
			})
		);
	},
	_renderLoader: function () {
		var loader = new Loader();
		loader.render();
		this.appsWrapper.append(loader.$el);
		Store.apps.on('afterload', function () {
			loader.remove();
		});
	},
	render: function () {
		this.$el.html(this.template());
		this.quickSearch.render();
		setTimeout(_.bind(function () {
			this.$el.addClass(Store.settings.isIPhone ? 'list' : 'grid');
			this.appsContainer = this.$el.find(".apps");
			this.appsWrapper = this.$el.find(".apps-wrapper");
			this.categoryFilter = this.$el.find("#apps-sort-fake");
			this.categorySelect = this.categoryFilter.find('select');

			this.markersContainer = this.$el.find('#markers');

			this.leftNav = this.$el.find('a.left-nav-button');
			this.rightNav = this.$el.find('a.right-nav-button');

			//For IPhone we have to place the quicksearch element in the header
			if (!Store.settings.isIPhone) {
				this.$el.find('.top-nav .right-nav-button').before(this.quickSearch.$el);
			} else {
				Store.header.wrapper.append(this.quickSearch.$el);
			}


			this._bindListeners();
			if (Store.apps.loaded === true) {
				this._renderCategories();
				this._renderApplications();
			} else {
				this._renderLoader();
				Store.apps.on('afterload', _.bind(this._renderCategories, this));
				Store.apps.on('afterload', _.bind(this._renderApplications, this));
			}
		}, this), 0);
		return this;
	},
	_bindListeners: function () {
		var self = this;
		this.categorySelect.on('change', function () {
			self.filterCategory($(this).val());
		});
		// if (!Store.settings.isWeb) {
		bindSwipe(this.appsContainer, 60,
			_.bind(this.nextPage, this), _.bind(this.prevPage, this));
		// }
	},
	prevPage: function (e) {
		e.preventDefault();
		e.stopPropagation();
		if (this.page > 0) {
			if (this.rightNav.hasClass('disabled')) {
				this.rightNav.removeClass('disabled');
			}
			this.page--;
			this._slideTo(this.page);
			setMarker.call(this, this.page);
			if (this.page === 0) {
				this.leftNav.addClass('disabled');
			}
		}
	},
	nextPage: function (e) {
		e.preventDefault();
		e.stopPropagation();
		if (this.page < this.maxPage) {
			if (this.leftNav.hasClass('disabled')) {
				this.leftNav.removeClass('disabled');
			}
			this.page++;
			this._slideTo(this.page);
			setMarker.call(this, this.page);
			if (this.page === this.maxPage) {
				this.rightNav.addClass('disabled');
			}
		}
	},
	_slideTo: function (page) {
		var containerWidth = this.appsWrapper.width(),
			trans = -this.page * containerWidth;
		if (Store.settings.isIE) {
			trans -= 1;
		} else {
			trans += 4;
		}
		if (hasTransform()) {
			this.appsContainer.css({
				'-webkit-transform': 'translate3d(' + trans + 'px,0,0)'
			});
		} else {
			this.appsContainer.css({
				'left': trans
			});
		}
	}
});
