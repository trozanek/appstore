var _              = require('underscore'),
	BaseView       = require('./BaseView.js'),
	App            = require('../models/App.js'),
	Templates      = require('../templates'),
	bindSwipe      = require('../helper/bind-swipe');

module.exports = BaseView.extend({
	tagName: 'div',
	template: Templates.ImageSlider,
	slideTmpl: _.template('<div class="img-wrapper"><img src="<%= src %>" alt="<%= alt %>" /></div>'),
	initialize: function () {
		this.app = this.options.app;
		this.screenshots = this.app.get('screenshots');
		this.current = 0;

		Store.on('orientationchange', _.bind(function () {
			this._reset();
			this._fitImages();
		}, this));
	},
	prevSlide: function (e) {
		if (this.current > 0) {
			this.current--;
			this._slideTo(this.current);
			this._setMarker(this.current);
		}
	},
	nextSlide: function (e) {
		if (this.current < this.screenshots.length - 1) {
			this.current++;
			this._slideTo(this.current);
			this._setMarker(this.current);
		}
	},
	_reset: function () {
		this.current = 0;
		this._setMarker(0);
		this._slideTo(0);
	},
	_slideTo: function (index) {
		var trans = - (index * this.slidesWrapper.width());
		if (Store.settings.isIE) {
			this.slides.css('margin-left', trans + 'px');
		} else {
			this.slides.css('-webkit-transform', 'translate3d(' + trans + 'px, 0, 0)');
			this.slides.css('-moz-transform', 'translate3d(' + trans + 'px, 0, 0)');
		}
	},
	_fitImages: function () {
		if (this.screenshots.length === 0) {
			return;
		}
		var maxWidth = this.slidesWrapper.width() - 20,
			imgWrapper = $('.img-wrapper');
		imgWrapper.css('width', this.slidesWrapper.width());
		this.slidesImages.each(function (index, img) {
			var width;
			img = $(img);
			width = img.width();
			if (width) {
				if (width > maxWidth) {
					img.css('width', maxWidth);
				}
			} else {
				img.load(function () {
					if (img.width() > maxWidth) {
						img.css('width', maxWidth);
					}
				});
			}
		});
	},
	_bindListeners: function () {
		this.slidesImages.on('dragstart', function (e) {
			e.preventDefault();
			return false;
		});

		this.leftButton.on('click', _.bind(this.prevSlide, this));
		this.rightButton.on('click', _.bind(this.nextSlide, this));

		bindSwipe(this.slides, 50,
				_.bind(this.nextSlide, this), _.bind(this.prevSlide, this));
	},
	_setMarker: function (index) {
		_.each(this.markers, function (marker, i) {
			marker[index === i ? 'addClass' : 'removeClass']('active');
		});
	},
	_renderMarkers: function () {
		var self = this;
		this.markersContainer.append.apply(
			this.markersContainer,
			this.markers = _.map(this.screenshots, function (scr, index) {
				return $('<a/>', {
					'class': index === 0 ? 'active' : ''
				});
			})
		);
	},
	render: function () {
		var self = this;
		if (this.screenshots.length === 0) {
			return this;
		}

		this.$el.html(this.template());
		setTimeout(_.bind(function () {
			this.slidesWrapper = this.$el.find(".screenshots-wrapper");
			this.slides = this.$el.find('.scrollable');
			_.each(this.screenshots, function (scr) {
				self.slides.append(self.slideTmpl(scr));
			});
			this.slidesImages = this.slides.find('img');
			this.leftButton = this.$el.find('.switchers a.left');
			this.rightButton = this.$el.find('.switchers a.right');
			this.markersContainer = this.$el.children('div.img-markers');
			//next callstack to read width properly
			setTimeout(_.bind(this._fitImages, this), 0);
			this._renderMarkers();
			this._bindListeners();
		}, this), 0);
		return this;
	}
});
