var _         = require('underscore'),
	BaseView  = require('./BaseView.js'),
	Loader    = require('./Loader'),
	Templates = require('../templates'),
	AppView   = require('./AppView'),
	helpers   = require('../helper'),
	toURI     = helpers.toURI,
	toITMSURL = helpers.toITMSURL,
	UpdatesView;

module.exports = BaseView.extend({
	id: 'updates',
	tagName: 'div',
	className: 'page',
	template: Templates.Updates,
	initialize: function () {
		this.appsViews = [];
		this.on('afterslide', function () {
			Store.header.setHomeButtonVis(true);
		});
	},
	render: function () {
		this.$el.html(this.template());
		setTimeout(_.bind(function () {
			this.appsWrapper = this.$el.find(".apps-wrapper");
			this.appsContainer = this.$el.find('.apps');
			this.updateAllButton = this.$el.find('.updates-btn');
			if (this.appsManager.loaded) {
				this._renderApplications();
				this._setUpdateAllButton();
			} else {
				this._renderLoader();
			}
			this._bindListeners();
		}, this), 0);
		return this;
	},
	_renderLoader: function () {
		var loader = new Loader();
		loader.render();
		this.appsWrapper.append(loader.$el);
		this.appsManager.on('afterload', function () {
			loader.remove();
		});
	},
	_bindListeners: function () {
		this.appsManager.on('change', _.bind(function () {
			this._renderApplications();
			this._setUpdateAllButton();
		}, this));
	},
	_renderApplications: function () {
		var appsManager = this.appsManager;

		this.appsViews.length = 0;
		Store.apps.each(_.bind(function (app) {
			var name = app.get("APPLICATION_NAME"), version = app.get("APPLICATION_VERSION");
			if (appsManager.isInstalled(name) && !appsManager.isUpToDate(name, version)) {
				this.appsViews.push(new AppView({
					app: app
				}));
			}
		}, this));

		if (this.appsViews.length === 0) {
			this.appsContainer.html('<li>No available updates</li>');
			return;
		}

		this.appsContainer.html('');

		this.appsContainer.append.apply(this.appsContainer,
			_.map(this.appsViews, function (appView) {
				return appView.render().$el;
			}));
	},
	_setUpdateAllButton: function () {
		var self = this, ipas, data;

		this.updateAllButton.addClass('disabled');
		this.updateAllButton.text('LOADING');
		this.updateAllButton.off('click');

		if (this.appsViews.length === 0) {
			this.updateAllButton.text("NO UPDATES");
			return;
		}

		ipas = _.map(this.appsViews, function (appView) {
			var app = appView.app;
			if (app) {
				return {
					'ipa_url' : toURI(app.get('APPLICATION_BUNDLE_URL')),
					'small_icon_url' : toURI(app.get('APPLICATION_ICON')),
					'full_size_icon_url' : toURI(app.get('APPLICATION_ICON').slice(0, -4) + '_512.png'),
					'bundle_identifier' : app.get('APPLICATION_BUNDLE_ID'),
					'bundle_version' : app.get('APPLICATION_BUNDLE_VERSION') || '',
					'title' : app.get('APPLICATION_NAME')
				};
			}
		});

		data = {result: {'ipas': ipas}};
		$.ajax({
			dataType: 'jsonp',
			data: {json: JSON.stringify(data)},
			crossDomain: true,
			timeout: 10000,
			url: Store.settings.plistServicePath,
			success: function (data) {
				console.log('IPA PLIST GEN DATA', data);
				self.updateAllButton.text("UPDATE ALL");
				self.updateAllButton.on('click', function () {
					self.appsManager.setInstalled(_.map(self.appsViews, function (appView) {
						return {
							name: appView.app.get('APPLICATION_NAME'),
							version: appView.app.get('APPLICATION_VERSION')
						};
					}));
					window.location.href = toITMSURL(data.url);
				});
				self.updateAllButton.removeClass('disabled');
				console.log("Go to: " + toITMSURL(data.url));
				//
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log('err ' + xhr.statusText);
			}
		});

	}
});
