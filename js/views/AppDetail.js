var _           = require('underscore'),
	BaseView    = require('./BaseView.js'),
    App         = require('../models/App.js'),
    Templates   = require('../templates'),
    ImageSlider = require('./ImageSlider.js'),
    Loader      = require('./Loader'),
    setBtnCls   = require('../helper/set-button-class');

module.exports = BaseView.extend({
	id: 'appdetails',
	tagName: 'div',
	className: 'page',
	template: Templates.AppDetail,
	initialize: function () {
		this.appId = this.options.appId;
		this.slider = null;

		this.on('afterslide', _.bind(function () {
			// Store.header.setHomeButtonVis(true);
			if (this.slider) {
				this.slider._fitImages();
			}
		}, this));
	},
	_fill: function () {
		var self = this, apps;

		this.app = Store.apps.find(function (app) {
			return app.get('APPLICATION_NAME') === self.appId;
		});
		if (!this.app) {
			this._render404();
		} else {
			this._renderApp();
		}
	},
	_renderLoader: function () {
		var loader = new Loader();
		loader.render();
		this.$el.append(loader.$el);
		Store.apps.on('afterload', function () {
			loader.remove();
		});
	},
	_render404: function () {
		this.$el.html("404 Application not found!");
	},
	render: function () {
		if (!Store.apps.loaded) {
			this._renderLoader();
			Store.apps.on('afterload', _.bind(this._fill, this));
		} else {
			this._fill();
		}
		return this;
	},
	_renderApp: function () {
		this.slider = new ImageSlider({
			app: this.app
		});
		this.$el.html(this.template(this.app.toJSON()));
		this.slider.render();
		setTimeout(_.bind(function () {
			this.button = this.$el.find('.install-button');
			this.$el.find('#app-details').append(this.slider.$el);
			if (this.appsManager.loaded) {
				this._setButtonText();
			} else {
				this.appsManager.on('afterload', _.bind(this._setButtonText, this));
			}
			this._bindListeners();
		}, this), 0);
		return this;
	},
	_bindListeners: function () {
		var self = this, install = _.bind(function () {
			this.appsManager.setInstalled(
				this.app.get('APPLICATION_NAME'),
				this.app.get('APPLICATION_VERSION')
			);
			this._setButtonText();
			window.location.href = self.app.get('APPLICATION_INSTALL_URL');
		}, this);

		this.button.on('click', function (e) {
			e.preventDefault();
			if (Store.settings.isWeb) {
				Store.overlay.show(self.app, install);
			} else {
				install();
			}
			return false;
		});
	},
	_setButtonText: function () {
		var appsManager = this.appsManager, appName = this.app.get('APPLICATION_NAME'),
			appVer = this.app.get('APPLICATION_VERSION');

		if (appsManager.isInstalled(appName)) {
			if (!appsManager.isUpToDate(appName, appVer)) {
				setBtnCls(this.button, 'update-button');
				this.button.text(this.app.get('UPDATE_TEXT'));
			} else {
				setBtnCls(this.button, 'reinstall-button');
				this.button.text(this.app.get('REINSTALL_TEXT'));
			}
		} else {
			setBtnCls(this.button, '');
			this.button.text(this.app.get('INSTALL_TEXT'));
		}
	}
});
