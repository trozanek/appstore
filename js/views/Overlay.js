var _         = require('underscore'),
	Templates = require('../templates'),
	BaseView  = require('./BaseView.js'),
	App       = require('../models/App.js');

module.exports = BaseView.extend({
	className: 'overlay',
	tagName: 'div',
	template: Templates.Overlay,
	initialize: function () {
		this.app = null;

		Store.on('pagechange', _.bind(this.hide, this));
	},
	render: function () {
		this.$el.html(this.template());
		setTimeout(_.bind(function () {
			this.downloadLink = this.$el.find('a.download');
			this.cancelLink = this.$el.find('div.footer a');
			this._bindListeners();
		}, this), 0);
		return this;
	},
	_bindListeners: function () {
		this.downloadLink.on('click', _.bind(function (e) {
			e.preventDefault();
			var app = this.app, onDownload = this.onDownload;
			if (app && onDownload) {
				this.hide();
				onDownload(app);
			}
			return false;
		}, this));

		this.cancelLink.on('click', _.bind(function (e) {
			e.preventDefault();
			if (this.app && this.onCancel) {
				this.onCancel(this.app);
			}
			this.hide();
			return false;
		}, this));
	},
	show: function (app, onDownload, onCancel) {
		this.app = app;
		this.onDownload = onDownload;
		this.onCancel = onCancel;
		this.$el.css('display', 'block');
	},
	hide: function () {
		this.$el.css('display', 'none');
		this.app = null;
		this.onDownload = null;
		this.onCancel = null;
	}
});
