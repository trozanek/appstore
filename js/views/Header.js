var  _            = require('underscore'),
	BaseView      = require('./BaseView.js'),
	UpdatesButton = require('./UpdatesButton'),
	Templates     = require('../templates'),
	Header;

module.exports = BaseView.extend({
	tagName: 'div',
	id: 'store-header',
	initialize: function () {
		this.updatesButton = new UpdatesButton();
	},
	render: function () {
		this.$el.html(this.template());
		if (Store.settings.updateAll !== false) {
			this.updatesButton.render();
		}

		setTimeout(_.bind(function () {
			this.wrapper = this.$el.children('div.wrapper');
			this.homeButton = this.wrapper.children('a.btn-home');
			this.searchButton = this.wrapper.children('a.search-button');
			if (Store.settings.updateAll !== false) {
				this.wrapper.append(this.updatesButton.$el);
			}

			this._bindListeners();

		}, this), 1);
		return this;
	},
	_bindListeners: function () {
		var self = this;
		if (Store.settings.isIPhone) {
			this.searchButton.on('click', function () {
				self.wrapper.addClass('quicksearch');
			});
		}
	},
	setHomeButtonVis: function (vis) {
		if (!this.homeButton) {
			this.homeButton = this.$el.find('a.btn-home');
		}
		this.homeButton[vis ? 'show' : 'hide']();
	},
	setSearchVis: function (vis) {
		if (!this.searchButton) {
			return;
		}
		this.searchButton[vis ? 'show' : 'hide']();
		if (!vis) {
			this.wrapper.removeClass('quicksearch');
		}
	},
	template: Templates.Header
});
