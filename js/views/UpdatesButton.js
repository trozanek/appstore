var  _        = require('underscore'),
	BaseView  = require('./BaseView.js'),
	Templates = require('../templates'),
	Header;

module.exports = BaseView.extend({
	tagName: 'a',
	className: 'updates-btn top-bar-btn',
	template: Templates.UpdatesButton,
	initialize: function () {
		this.disabled = true;
	},
	render: function () {
		this.$el.html(this.template());
		this.$el.attr('href', '#updates');
		setTimeout(_.bind(function () {
			this.number = this.$el.children('div');
			this.text = this.$el.children('span');
			this._bindListeners();
			this.disable();
			this.text.text("LOADING");
		}, this), 1);
		return this;
	},
	_bindListeners: function () {
		this.appsManager.on('change', _.bind(function () {
			var count = 0, appsManager = this.appsManager;
			Store.apps.each(function (app) {
				var name = app.get('APPLICATION_NAME'),
					version = app.get('APPLICATION_VERSION');

				if (appsManager.isInstalled(name) && !appsManager.isUpToDate(name, version)) {
					count++;
				}
			});
			this[(count > 0 ? 'enable' : 'disable')](count);

		}, this));
	},
	enable: function (count) {
		this.number.text(count);
		this.text.text('UPDATES');
		this.$el.removeClass('disabled');
	},
	disable: function () {
		this.number.text('0');
		this.text.text('NO UPDATES');
		this.$el.addClass('disabled');
	}
});
