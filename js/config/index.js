var helper    = require('../helper'),
	getCookie = require('../helper/get-cookie');

var settings = {
	isDev: false,
	isQA: false,
	isUA: false,
	isProd: false,
	devKey: 'd4474cf2a5ab61971196763441d',
	prodKey: 'fc447464202c7ab312062dff866',
	ESBBase: '',
	b2bDev: 'https://b2bdev.gene.com/esb/invoke/GneENT_Dispatcher.Services:invokeDispatcher',
	b2bProd: 'https://b2b.gene.com/esbDispatcher',
	b2bQA: 'https://b2bqa.gene.com/esbitDispatcher',
	b2bUA: 'https://b2bua.gene.com/esbDispatcher',
	//dataURL: '',
	isIPad: false,
	isIPhone: false,
	isWeb: false,
	isIE: false,

	//===

	updateAll: true,
	oldTypeUrls: false,

	// client will be iphone, ipad, or desktop
	client: '',
	isRoche: false,
	isGenentech: false,
	isInternal: false,
	rocheProvisionURL: 'appdist/Roche2011.mobileprovision',
	geneProvisionURL: 'appdist/Genentech2011.mobileprovision',
	rocheAppListURL: 'data/apps_list_roche.json',
	geneAppListURL: 'data/apps_list.json',
	appListURL: '',
	userid: '',
	bannersMobileRochePath: 'data/banners_iphone_roche.json',
	bannersMobileGenePath: 'data/banners_iphone.json',
	bannersRochePath: 'data/banners_roche.json',
	bannersGenePath: 'data/banners.json',
	bannersPath: '',
	plistServicePath: 'http://ipaserver01.gene.com:4567/generatePlist',
	logoPath: '',
	logoPathRoche: 'img/roche-logo.png',
	logoPathGene: 'img/gene-logo.png',
	logoPathRocheRetina: 'img/roche-logo@2x.png',
	logoPathGeneRetina: 'img/gene-logo@2x.png',
	features: {
		search: true,
		searchMobile: true,
		updateAll: true,
		updateAllMobile: true
	}
};

var agent = navigator.userAgent.toLowerCase();

// dynamic settings
var subdomain = location.hostname.split('.')[0];
settings.isDev = (subdomain.indexOf('dev') !== -1) ? true : false;
settings.isQA = (subdomain.indexOf('qa') !== -1) ? true : false;
settings.isUA = (subdomain.indexOf('ua') !== -1) ? true : false;
settings.isProd = (!settings.isDev && !settings.isQA && !settings.isUA) ? true : false;

//====

settings.updateAll = settings.isUA ? false : true;
settings.oldTypeUrls  = settings.isUA ? true : false;

// api keys - ua and prod use prodKey and dev and qa use devKey
settings.apiKey = (!settings.isDev && !settings.isQA) ? settings.prodKey : settings.devKey;

// choose ESBBase depending on environment
if (settings.isDev) { settings.ESBBase = settings.b2bDev; }
if (settings.isQA) { settings.ESBBase = settings.b2bQA; }
if (settings.isUA) { settings.ESBBase = settings.b2bUA; }
if (settings.isProd) { settings.ESBBase = settings.b2bProd; }

settings.isIPad = (agent.indexOf("ipad") !== -1);
settings.isIPhone = (agent.indexOf("iphone") !== -1);
settings.isWeb = (!settings.isIPhone && !settings.isIPad);
settings.client = (settings.isIPhone) ? 'iphone' : (settings.isIPad) ? 'ipad' : 'web';
settings.isIE = /msie/i.test(navigator.userAgent);

settings.isGenentech = (location.hostname.indexOf('roche') === -1);
settings.isRoche = (location.hostname.indexOf('roche') !== -1);

if (settings.isRoche) {
	document.title = "Roche Appstore";
}

if (settings.isGenentech && settings.isIPhone) { settings.bannersPath = settings.bannersMobileGenePath; }
if (settings.isRoche && settings.isIPhone) { settings.bannersPath = settings.bannersMobileRochePath; }
if (settings.isGenentech && !settings.isIPhone) { settings.bannersPath = settings.bannersGenePath; }
if (settings.isRoche && !settings.isIPhone) { settings.bannersPath = settings.bannersRochePath; }

settings.provisionURL = (settings.isRoche) ? settings.rocheProvisionURL : settings.geneProvisionURL;
settings.appListURL = (settings.isRoche) ? settings.rocheAppListURL : settings.geneAppListURL;

if (settings.isRoche) {
	settings.logoPath = (window.devicePixelRatio && window.devicePixelRatio >= 2) ?
		settings.logoPathRocheRetina : settings.logoPathRoche;
}
if (settings.isGenentech) {
	settings.logoPath = (window.devicePixelRatio && window.devicePixelRatio >= 2) ?
		settings.logoPathGeneRetina : settings.logoPathGene;
}
// unfortunately there are two diff kinds of ids?
var idtest1 = getCookie('user_id1');
var idtest2 = getCookie('user_id2');

if (idtest1 && idtest1.length > 0) {
	settings.userid = idtest1;
}
if (idtest2 && idtest2.length > 0) {
	settings.userid = idtest2;
}

module.exports = settings;
