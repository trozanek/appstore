var Backbone    = require('backbone'),
    _           = require('underscore'),
    AppDetail   = require('../views/AppDetail.js'),
    Home        = require('../views/Home.js'),
    Updates     = require('../views/Updates.js'),
    Router, cache = {}, getFromCache;

getFromCache = function (name) {
	if (cache[name]) {
		if (cache[name].removed === true) {
			delete cache[name];
		} else {
			cache[name].fromCache = true;
		}
	}
	return cache[name] || null;
};

module.exports = Backbone.Router.extend({
	initialize: function () {

	},
	routes: {
		"": "home",
		"apps/:app": "app",
		"updates": "updates"
	},
	app: function (app) {
		var id = "apps-" + app, view;
		view = getFromCache(id);
		if (!view) {
			cache[id] = view = new AppDetail({ appId: app });
		}
		Store.setPage(view);
	},
	home: function (page) {
		var home = getFromCache('Home');
		if (!home) {
			cache.Home = home = new Home();
		}
		Store.setPage(home, -1);
	},
	updates: function () {
		var updates = getFromCache('Updates');
		if (!updates) {
			cache.Updates = updates = new Updates();
		}
		Store.setPage(updates);
	}
});
