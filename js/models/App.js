var Backbone      = require('backbone'),
	_             = require('underscore'),
	helper        = require('../helper'),
	toRelativeURL = helper.toRelativeURL,
	toITMSURL     = helper.toITMSURL,
	App;

App = Backbone.Model.extend({
	initialize: function (obj) {
		var self = this;

		// clean urls from package manager
		// TODO: add urls for 2x image size for retina display
		// TODO: exit function if any of the below are undefined

		this.set({
			id: this.get('APPLICATION_NAME'),
			INSTALL_TEXT: 'install',
			REINSTALL_TEXT: 'reinstall',
			UPDATE_TEXT: 'update',
			APPLICATION_ICON : toRelativeURL(this.get('APPLICATION_ICON')),
			APPLICATION_SCRSHOT_URL : toRelativeURL(this.get('APPLICATION_SCRSHOT_URL'))
		});

		var path = this.get('APPLICATION_ICON');
		var fullIcon = path.replace('.png', '_175.png');
		this.set({APPLICATION_ICON_LARGE: fullIcon});

		if (!Store.settings.isIPad && !Store.settings.isIPhone) {
			this.set({APPLICATION_INSTALL_URL: this.get('APPLICATION_BUNDLE_URL')});
		} else {
			if (Store.settings.oldTypeUrls === true) {
				this.set({APPLICATION_INSTALL_URL: toITMSURL(
					this.get('APPLICATION_PLIST_URL').indexOf('appdist2') > -1 ?
						'https://' + document.location.hostname + this.get('APPLICATION_PLIST_URL') :
						'https://' + document.location.hostname + document.location.pathname + this.get('APPLICATION_PLIST_URL')
				)});
			} else {
				this.set({APPLICATION_INSTALL_URL: toITMSURL(this.get('APPLICATION_PLIST_URL'))});
			}
		}

		this.set({ screenshots : [] });
		this.set({ PROVISION_URL: Store.settings.provisionURL });
		this.set({ FORMATTED_DATE: this.get('LAST_MODIFIED').slice(0, 10) });

		if (this.get('APPLICATION_SCRSHOT_URL')) {
			var urls = this.get('APPLICATION_SCRSHOT_URL').split(';');
			var screenshot;
			_.each(urls, function (url) {
				if (url.length > 0) {
					screenshot = {
						src: toRelativeURL(url),
						alt: self.get('APP_DISPLAYNAME'),
						width: '230',
						height: '345'
					};
					self.get('screenshots').push(screenshot);
				}
			});
		}
	},
	filterByCategory: function (category) {
		if (category === 'All') {
			return true;
		}
		return this.get('APP_TAGS') === category;
	},
	filterBySearch: function (text) {
		if (text === '') {
			return true;
		}
		return (this.get('APP_DISPLAYNAME') + ' ' +
			this.get('APPLICATION_DESCRIPTION')).toLowerCase().indexOf(text.toLowerCase()) > -1;
	}
});

module.exports = App;
