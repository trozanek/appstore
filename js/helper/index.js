// a collection of random helper functions that need to be separated out

//https://gist.github.com/901295/904de6b03dcfe1f9d2d100d1f5721c1ecb2746ed
//fixes an older scrolling bug on ios devices
(function (doc) {
	var addEvent = 'addEventListener', type = 'gesturestart', qsa = 'querySelectorAll',
		scales = [1, 1], meta = qsa in doc ? doc[qsa]('meta[name=viewport]') : [], fix;

	fix = function () {
		meta.content = 'user-scalable=no,width=device-width,minimum-scale=' +
			scales[0] + ',maximum-scale=' + scales[1];
		doc.removeEventListener(type, fix, true);
	};

	if ((meta = meta[meta.length - 1]) && addEvent in doc) {
		fix();
		scales = [0.25, 1.6];
		doc[addEvent](type, fix, true);
	}
}(document));

// Stuff for IE
if (!(window.console || false)) {
	window.console = {
		//no-op functions
		log : function () {},
		warn : function () {},
		dir : function () {}
	};
}
// need to consider putting these into something else like a util object
var toRelativeURL = function (url) {
	// works only as tested with apps that use
	// /webcentral/iphoneApps/appstore/apps/... urls
	if (url) {
		var paths = url.split('/');
		var index = paths.indexOf('appstore');
		if (index !== -1) {
			return paths.slice(index + 1).join('/');
		} else {
			return url;
		}
	}
	// this will just return an empty string in the case of no data
	// links will be broken but the app will not break
	return "";
};
var toURI = function (path) {
	return location.protocol + '//' + location.hostname + location.pathname + path;
};

var toITMSURL = function (path) {
	return 'itms-services://?action=download-manifest&url=' + path;
};

var getCookie = function (value) {
	var cookie = document.cookie, index = cookie.indexOf(value);
	////console.log(document.cookie);
	if (index !== -1) {
		var str = cookie.substring(index + value.length + 1, cookie.indexOf(';', index));
		return str;
	}
	return false;
};
// transition test from here:
// https://github.com/louisremi/jquery.transition.js/blob/master/jquery.transition.js
var hasTransitions = function () {
	var div = document.createElement("div"), divStyle = div.style, trans = "Transition";
	// Only test for transition support in Firefox and Webkit
	// as we know for sure that Opera has too much bugs (see http://csstransition.net)
	// and there's no guarantee that first IE implementation will be bug-free
	return "Moz" + trans in divStyle ? "Moz" + trans:
	"Webkit" + trans in divStyle ? "Webkit" + trans:
	false;
};

if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function (obj, start) {
		for (var i = (start || 0), j = this.length; i < j; i++) {
			if (this[i] === obj) { return i; }
		}
		return -1;
	};
}

var orientationchange = function () {
	var o = window.orientation;
	$('body').removeClass('portrait').removeClass('landscape');
	if (o === 0 || o === 180) {
		$('body').addClass('portrait');
	} else {
		$('body').addClass('landscape');
	}
};

window.onorientationchange = orientationchange;
orientationchange();

module.exports = {
	hasTransitions : hasTransitions,
	getCookie : getCookie,
	toITMSURL : toITMSURL,
	toURI : toURI,
	toRelativeURL : toRelativeURL
};
