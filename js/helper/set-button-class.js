module.exports = function (button, cls) {
	button.removeClass('reinstall-button');
	button.removeClass('update-button');
	button.addClass(cls);
};
