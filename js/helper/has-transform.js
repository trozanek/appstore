module.exports = (function () {
	var el = document.createElement('div'),
	has = el.style.webkitTransform !== undefined;
	return function () {
		return has;
	};
}());
