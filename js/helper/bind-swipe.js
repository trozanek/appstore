var _ = require('underscore');

module.exports = function (target, distance, onLeft, onRight) {
	var startX = 0, started = false;
	distance = distance || 100;

	if (Store.settings.isIE) {
		return;
	}
	target[0].addEventListener('touchstart', function (e) {
		// e.preventDefault();
		var touch = e.targetTouches[0];
		if (touch) {
			started = true;
			startX = touch.pageX;
		}
	});

	target[0].addEventListener('touchmove', function (e) {
		if (started && e.changedTouches[0] && Math.abs(startX - e.changedTouches[0].pageX) > 10) {
			e.preventDefault();
			return false;
		}
	});

	target[0].addEventListener('touchend', function (e) {
		var touch = e.changedTouches[0], dist;
		if (started && touch) {
			dist = touch.pageX - startX;
			if (Math.abs(dist) >= distance) {
				e.preventDefault();
				if (dist < 0) {
					onLeft(e);
				} else {
					onRight(e);
				}
			}
		}
	});

	target[0].addEventListener('touchcancel', function (e) {
		e.preventDefault();
		started = false;
		startX = 0;
	});
};
